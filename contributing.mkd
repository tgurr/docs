Title: Contributing
CSS: /css/main.css

#{include head}

Contributing Documentation
==========================
- See [updating and writing documentation](documentation.html)

Contributing Code
=================

* This will become a table of contents (this text will be scraped).
{:toc}

## First Steps

Exherbo uses [git](http://git-scm.com) for source code management (SCM) and its
code is maintained on our [Gitlab] instance.

- If you don't already have Git set up:

      git config --global user.name "<your real name>"
      git config --global user.email "<your email>"


- Register an account on our [Gitlab] instance, see the
  [docs](/docs/gitlab.html) for a how-to.

- Fork the repository you want to change things in and clone it from [Gitlab].
  See ['Cloning repositories'](/docs/gitlab.html#cloning_repositories).

## Workflow for contributing to package repositories

This is directly lifted from the work of Ciaran McCreesh and can be found in its original form on
[his blog](//ciaranm.wordpress.com/2010/11/28/exherbo-development-workflow-version-2/ "Ciaran McCreesh's blag - Exherbo Development Workflow, Version 2").
This will be updated to match changes there as possible.

Repositories are configured as normal, with their `sync` set to point to the usual remote location.
The location is specified in `/etc/paludis/repositories/<repo>.conf` like this:

    sync = git+https://git.exherbo.org/<repo>.git

To contribute on a repository, first fork it and pull a local clone. Fork
these repositories on our [Gitlab instance](https://gitlab.exherbo.org) to
easily push your contributions. In the following explanation we will use arbor,
but it works with any repository.

Now that we have a local clone of the fork in `/home/users/ciaranm/repos/arbor`,
change `/etc/paludis/repositories/arbor.conf` to:

        sync = git://git.exherbo.org/arbor.git local: git+file:///home/users/ciaranm/repos/arbor

Normally, when you sync, you'll be syncing against upstream. To sync with the
local clone:

        cave sync -s local arbor

The workflow now is:
- Update the local copy of your personal repository to be in sync with upstream.
  It possibly saves you from rebasing your work on changes that happened in the
  meantime. Or even worse, somebody might already have done the exact same thing.
- Work locally on your changes until everything works. The package should
  resolve fine on your system.
- Use the wonders of `git rebase -i` to tidy up your work into nice friendly
  pushable commits.
- Push the commits to your remote and let Gitlab CI runs the tests.
- If tests pass, create a merge request on [Gitlab].
- Update the MR until it gets accepted.
- Go back to syncing without the suffix.

Some things to note:

- This only really works with Git, and only when using the default 'reset' sync mode.
- You're never manually modifying any repository which Paludis also modifies.
- You can use `git commit --fixup <commit-id>` for easier commit squashing.
- It's usually better to do _one_ thing with _one_ commit.
  E.g. if you version bump a package and also implement style improvements you
  may want to do _two_ commit for those _two_ changes.
- You can use `git rebase -i --autosquash` if you used `fixup` commits.
- This will automatically squash `fixup` commits into the commits they're fixing.
- When creating merge request on [Gitlab], check the box **Allow edits from
  maintainers**. This allows core devs to rebase your merge request, which is
  required for fast-forward merges if HEAD has changed since you had submitted
  your merge request. 
- Sometimes CI tests fail because packages' tests are broken upstream, continue
  with the merge request in this case.
- The suffix things work on `sync_options` too, if you need it. Thus, for branches, you can use `sync_options = --branch=branch-on-upstream local: --branch=my-local-branch`
- In case you like to use multiple branches at once or have your branches named differently depending
  on what topic they deal with and don't want to modify the repository config file everytime you can use 
  `cave sync -s local <repo> -r <commit-id>`. You don't have to supply the
  full commit-id, you only need to provide at least the first 4 characters.
  Also keep in mind that when you amend a commit its commit-id changes, if you
  keep using the old commit-id you'll sync the old version of that commit.

## Style requirements

- Configure your git author name and email properly. Use your real name.
  See [`man git-config`](http://schacon.github.com/git/git-config.html), look for
  `user.name` and `user.email`.

- Generate your patches using `git format-patch`, from local commits. If your patch renames files,
  please use the options `-M -C -C` to `git format-patch`, to detect copies and renames. This makes
  the resulting patch much easier to read. Alternatively, you can enable `-M -C` by default by
  running `git config --global diff.renames copy`.  See [`man
  git-format-patch`](http://schacon.github.com/git/git-format-patch.html).

- Use a `clear and descriptive commit message`, explaining the change.

  - Write a `short one line summary`, followed by an `empty line`, and, as you see fit, a more
    `elaborate description` of your changes

  - `Version bump` is not an acceptable commit message. You should at least mention the package and
    version you're adding. Try running `git log --oneline` on a repository full of `Version bump`
    commits and you'll understand why.

  - In case you're making non-obvious changes, consider explaining the reasoning behind the changes.

  - If you rearrange code or make white space and style changes then avoid making real changes in
    the same commit. Put the actual changes you do in a separate commit. This makes it easier to
    review as well as allowing git to track the history of the affected code properly.

  - Do make references to relevant bug reports, mailing lists, ... where appropriate.

- Exhereses use 4 spaces and strictly no tabs for indentation. Run your patch through `git diff
  --check`, to verify that it doesn't introduce whitespace errors.

- Install and use an [editor plugin](editor-plugins.html) with exheres support.

## Submitting patches for package repositories

- When submitting a new package, first consider whether it's really important enough to be in the
  official repositories, or whether you should instead put it in your own supplemental repository.
  If the quality of your supplemental repository is reasonably high, host it somewhere 
  (preferably on our [Gitlab instance](//gitlab.exherbo.org/exherbo)), and
  we'll consider adding it to the unavailable-unofficial repository.

- Do submit original work. Don't blindly copy ebuilds, adjusting for differences between ebuilds
  and exhereses. If you need to install a package, there is a tool far more suitable that you can
  use instead of blindly adapting ebuilds to exheres.

- Don't try to bump large and critical packages (e.g gcc, X, grub...) on your own without speaking
  to their maintainers. Bumping such packages takes some time and requires extra testing, so
  please be patient.

- Sometimes patching upstream sources is unavoidable. If you do apply patches, make sure they're
  properly documented according to our patches policy, found in
  [exheres-for-smarties](eapi/exheres-for-smarties.html#applying_patches). Always consider that adding
  patches that will not be accepted upstream means you get to maintain those.

- To be included in any of our repositories, contributions must be licenced under the GPL-2. Add an
  appropriate copyright notice if your contributions to a given file warrant it.
  See [exheres-for-smarties](eapi/exheres-for-smarties.html#copyright_lines).

- Patches should be contributed via our [Gitlab](//gitlab.exherbo.org/exherbo).
  See our [Gitlab docs](/docs/gitlab.html) for more information.

## Submitting your repository for inclusion in unavailable-unofficial

If you have your own repository and its quality is fairly high, you should submit it for review so
it will be included in unavailable-unofficial.
To do that, just add it to our infra-scripts git repository as in this [example](repository-add-example.txt)
and push it to Gitlab.

This way, others will not duplicate any work you may
have done, and you will get helpful tips on how to improve your packaging skills. Also, this will
allow others to more easily contribute to your packages or point out bugs. See kloeri's
[blog post](http://kloeri.livejournal.com/11205.html) if you are not yet convinced.

We expect a decent level of responsiveness from you and we reserve the right to remove repositories
with broken packages if you do not respond to patches and inquiries within a reasonable amount of
time (Rule of thumb: within 30 days).

More concretely, if there's a patch for your repository for 30 days or more, we will consider your
repository for removal from unavailable-unofficial.
You'll get up to two additonal "wake-up calls" before the actual removal, though. If you're not on
IRC, we'll send you an email to your last known address (so make sure to use a valid, working email
address in your exheres, etc.).

This is *not* about "punishing" you or something like that but to ensure that the repositories in
unavailable-unofficial are maintained according to Exherbo's quality standards.

Remember that it is trivial to have your repository re-added once it is fixed up/maintained properly
again.

### Exherbo hook

To have fancy IRC commit notifications, please consider using our push hook. We currently support
GitHub, Gitorious and self-hosted repositories. Ask any Exherbo developer on IRC to get the WebHook
URL.

## Notes

Some of the guidelines outlined in this document are based upon `Documentation/SubmittingPatches`,
as part of [git]( https://github.com/git/git/blob/master/Documentation/SubmittingPatches)

[Gitlab]:               https://gitlab.exherbo.org/

--
Copyright 2009 Ingmar Vanhassel
Copyright 2013 Ciaran McCreesh, A Frederick Christensen

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
