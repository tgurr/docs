Title: Editor Plugins
CSS: /css/main.css

#{include head}

Editor Plugins
==============

* This will become a table of contents (this text will be scraped).
{:toc}

The packages listed here:
  - Provide syntax highlighting in your exheres
  - Flag common syntax errors
  - Fill in some standard metadata keys if you start writing a new exheres or exlib
  - Apply most of our formatting rules for you

### Emacs

The package `app-emacs/exheres-mode` provides modes for exhereses,
exlibs, and eclectic configuration files.

#### exheres-mode and exlib-mode

These modes are exheres- and exlib-syntax aware, so that global
keywords are recognised; keywords from some exlibs are supported as
well.

On top of that, the function `insert-exheres-skeleton` is provided,
which is bound to `C-c C-n` by default. It inserts a skeleton for an
exheres. The name for the copyright header is taken from the
customisable variable `exheres-skeleton-realname`.

When an exheres is saved, the PLATFORMS field is sorted; a warning is
issued if the field does not exist.

When an exlib is saved, no PLATFORMS field should be present; a
warning is issued otherwise.

#### Suggested customisations

The following code makes `C-c C-p` in diff-mode insert a patch
header. For this as well, the variable `exheres-skeleton-realname` is
used.

	(eval-after-load "diff"
	  (progn
	    (require 'skeleton)
	    (define-skeleton insert-exherbo-patch-header-skeleton
	      "Inserts the standard Exherbo header for patches"
	      nil
	      "Source: " exheres-skeleton-realname "\n"
	      "Upstream: \n"
	      "Reason: ")
	    (require 'diff-mode)
	    (define-key diff-mode-map "\C-c\C-p" 'insert-exherbo-patch-header-skeleton)))

### Vim

The package `app-vim/exheres-syntax` provides syntax highlighting for exhereses, along with an exheres/exlib template.

For other installation tips and more information, please refer to the [plugin repository](https://gitlab.exherbo.org/exherbo-misc/exheres-syntax).
