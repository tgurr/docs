Title: Expectations
CSS: /css/main.css

#{include head}

What we expect from developers (that means you)
===============================================

* First of all you need to understand that using Exherbo is an active commitment.
We fully expect you to help with development or other parts of the project. In
return you get to have lots of fun, learn some new things and will generally
have a much easier time getting help as you already know the workflow in
Exherbo.

* Secondly you're expected to read relevant documentation including everything on
[Exherbo](../documentation.html) and relevant [man
pages](//www.kernel.org/doc/man-pages/) when running into problems. Pay attention to news items
and the [exherbo-dev](http://lists.exherbo.org/mailman/listinfo/exherbo-dev) mailing list. If you
can't solve your problem by reading documentation we'll be happy to help but we might yell at you if
you skip this crucial step.

* We also expect you to listen to other people when asking for help and do
exactly what they ask. When we ask for a full buildlog we really want the full
buildlog and not just the last 50 lines before the error message. You can save
everybody a lot of pain if you follow other people's advice exactly.

* You're to think for yourself. Exherbo is not a system where we document
everything so you'll need a basic understanding of how Linux systems work to
be able to set up many things and work out problems efficiently when
necessary.

* Use IRC. Our primary means of communication is through the #exherbo channel on
[freenode](//www.freenode.net) and it tends to be much harder working with
the community outside IRC. You don't have to be online constantly but please
try to stay around for an hour or two. We'll get to know each other a little
better, which makes it easier to work together in the future whether you need
some bug fixed or want to contribute something to our main package repositories
or our website.

* Don't be afraid to speak up about problems. Exherbo is supposed to be a fun project so it's
important that problems are solved quickly before turning into something bigger. Other contributors
are generally happy to help with any problem they can as long as you make an effort yourself.

* Give back to the community. The community is giving you a lot more than just the bits and bytes
making up packages. All the help you get, the things you learn from Exherbo and the constant
improvements are due to the community. A good way of showing your appreciation is simply by trying
to understand the rules governing the community and trying to follow as best you can.

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
