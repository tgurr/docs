Title: Features
CSS: /css/main.css

#{include head}

Philosophy
==========
  Exherbo aims to be a stable and flexible distribution for developers and tinkerers, people who need their distribution to help them be productive.  Exherbo actually makes it fun for developers to develop, be it on their own code or the distribution itself.  A good set of tools, clear, up-front configuration and excellent support for decentralized development make the system work *for* instead of *against* the developer. Other features that Exherbo values include:


**A small core development team**  
  A small team is one that can adapt quickly to changes and keep focus on the philosophy of Exherbo. The downside to this is that a team of ~20 cannot maintain 2000+ packages. Exherbo solves this issue by offering robust distributed repository management, opting for many small repositories that integrate seamlessly with everyday management.

**Flexibility where it makes sense**  
  Paludis can work with both source-based and binary packages on equal terms, even if there is no defined exheres-based packages for them! Finally you can get the speed of a binary distribution with the control of a source-based one.

**Simple, safe account management**  
  Safe and tracked account management, with users and groups managed by the system so you don't have to worry about them.

**Tracking unwritten packages**  
  With the unwritten repository, packages that haven't been made can be tracked by the distribution itself, speeding up workflow.

**Alternatives**  
  If program Y does everything X does, but better, Exherbo can use Y instead of X in all cases.

--
Copyright 2008, 2009 Jonathan Dahan

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
