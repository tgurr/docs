How we want to do multilib and what we've done so far in the multilib branches

Preface

- Please don't get yourself worked up on terms. If we're calling it "multibuild
classes", feel free to call it ABI, BLOWS, GLOWS or whatever suits you.

- Currently, we're concentrating on what we call the "C" multibuild class, meaning
the "classical" multilib which is supported by glibc and gcc. Other kinds of ABIs
(e. g. Perl) will work similarly but we haven't gotten there fully yet.

- When we're speaking of "exheres", we're doing this for brevity's sake and *not*
to exclude exlibs.

- We're starting by explaining how we've started working on multilib for amd64
(x86_64) and x86 (i686) and how we're accomplishing things there. We'll get more
abstract later on.


I. Proof of concept

1. The profile
We've created the x86_64-pc-linux-gnu profile which, most importantly, contains
make.defaults. [1]

In it, we define the currently supported multibuild classes which, at the time of
writing, is only the "C" class.

MULTIBUILD_CLASSES="C"

In the future, we'll hopefully support other classes like "perl", "python" and
"ruby" as well.

Next, we're defining the possible targets for each of the supported multibuild
classes and the respective default target:

MULTIBUILD_C_TARGETS="32 64"
MULTIBUILD_C_DEFAULT_TARGET="64"

Some variables for each multibuild class must not be changed by the user to ensure
the packages' build systems won't make wrong assumptions about the host and target
environments:

MULTIBUILD_C_LOCKED_VARS="CBUILD CHOST LIBDIR PKG_CONFIG_PATH"

There may also be mandatory variables which are specified as REQUIRED_VARS per
multibuild class:

MULTIBUILD_C_REQUIRED_VARS="CBUILD CHOST LIBDIR PKG_CONFIG_PATH"

All REQUIRED_VARS need to be defined for each multibuild class and its respective
targets in the profiles, e. g.:

MULTIBUILD_C_32_PROFILE_CBUILD="i686-pc-linux-gnu"
[...]
MULTIBUILD_C_64_PROFILE_CBUILD="x86_64-pc-linux-gnu"

This is currently verified by multibuild_sanity_check_profile in
multibuild.exlib but should probably be verified in Paludis' code specific to the
exheres-0 profiles layout.

To allow the user to specify his own C{,XX}FLAGS, we have introduced variables
that can be overridden per each multibuild class' target:

MULTIBUILD_C_32_USER_CFLAGS="-pipe -O2"
MULTIBUILD_C_32_USER_CXXFLAGS="-pipe -O2"
MULTIBUILD_C_64_USER_CFLAGS="-pipe -O2"
MULTIBUILD_C_64_USER_CXXFLAGS="-pipe -O2"

Some variables should stack:

MULTIBUILD_C_STACKED_VARS="CFLAGS CXXFLAGS"

These are merged based on e. g. MULTIBUILD_C_64_PROFILE_CFLAGS,
MULTIBUILD_C_64_USER_CFLAGS and CFLAGS in that order. Variables that don't
stack get overridden in that order instead.

On a multilib profile setting -march in CFLAGS or setting CHOST directly in
your bashrc will therefore fail since they get appended to or override CFLAGS
and CHOST for all C targets. For instance -march=core2 in your 32 bit CFLAGS
won't work too well. On the other hand -march=native probably will.

Since CHOST is a locked variable, setting that just plain won't work.

The remaining parts of make.defaults like ARCH, PLATFORM, options and suboptions
are basically identical to those of non-multilib profiles so they're being omitted
here.


2. multibuild.exlib
The new multibuild exlib [3] primarily contains functions to work with different
multibuild classes and targets and sets the environment up accordingly. The
multibuild_* functions not listed below are boring and should not be needed by
any exheres.

- multibuild_get_var()
Allows for fetching the value of a variable for a specified class in
MULTIBUILD_CLASSES and a target in MULTIBUILD_${class}_TARGETS. This gets
merged based on MULTIBUILD_${class}_${target}_PROFILE_${var},
MULTIBUILD_${class}_${target}_USER_${var} and ${var}.

For stacked variables, ${var} gets appended to all build targets that use said
variable, if specified in the user's bashrc. For other variables it overrides
instead of appending.

- multibuild_get_all_var()
Allows for fetching the value of a variable for a specified class for all
targets in MULTIBUILD_${class}_TARGETS.

For instance "multibuild_get_all_var C LIBDIR" will output "lib32 lib64" on a
multilib profile.

- multibuild_load_build_target()
Loads all variables provided for a specified build class and build target. For
instance multibuild_load_build_target C 64 sets CFLAGS to the merged value
that multibuild_get_var C 64 CFLAGS would also output.

- multibuild_save_build_target()
Saves the current build target's environment as defined by what was previously
loaded through multibuild_load_build_target. If the exheres appends to one of
these variables, the change will be restored if you later reload said build
target.

The value of MULTIBUILD_${class}_CURRENT_TARGET is used to determine
which target is being saved.

- multibuild_switch_build_target()
Switch the current build target through multibuild_save_build_target and
multibuild_load_build_target.


3. What we do in exheres
At the time of writing in our proof of concept, we're compiling/installing all
possible build targets for the C multibuild class. The following (simplified)
examples come from the binutils exheres [4].

In general, in each exheres phase, we work on all build targets of the respective
multibuild class (which currently boils down to the C class only):

3.1 src_unpack & src_prepare
We unpack the package's archive twice - once for each build target into
subdirs of ${WORK} or, if supported, use ECONF_SOURCE:

    local multibuild_c_target

    for multibuild_c_target in ${MULTIBUILD_C_TARGETS} ; do
        edo mkdir -p "${WORK}/${multibuild_c_target}"
        edo cd "${WORK}/${multibuild_c_target}"
        default
    done

Using ECONF_SOURCE and consequently unpacking only once and doing an out-of-source
build is preferred.

In the same manner, we enter all build targets' respective subdirs and do whatever
needs to be done in src_prepare for all of them sequentially.
If necessary, we use multibuild_switch_build_target() to switch between build targets.
This step becomes inevitable in later phases.

3.2 src_configure & src_compile
Similarly to previous phases, we're both changing to the respective build target's subdir
and here we have to multibuild_switch_build_target as shown below to make sure all build
targets get configured correctly. This is needed for things using ECONF_SOURCE, too.

    local multibuild_c_target

    for multibuild_c_target in ${MULTIBUILD_C_TARGETS} ; do
        multibuild_switch_build_target C ${multibuild_c_target}

        edo cd "${WORK}/${multibuild_c_target}/${PNV}"

        econf --enable-serial-configure --libdir=/usr/${LIBDIR}
    done

We're passing the respective build target's ${LIBDIR} here as defined in the profile's
make.defaults.

As done before, in src_compile, we're switching subdirs and compile each build target
separately.

3.3 src_install
Apart from doing the same build target subdir and multibuild_switch_build_target()
switching as before, we may have to consider environment issues, e. g. adding library
search paths to /etc/ld.so.conf:

        insinto /etc/env.d/binutils
        hereins ${CTARGET}-${PV} << EOF
TARGET="${CTARGET}"
VER="${PV}"
LIBPATH="/usr/${CTARGET}"
FAKE_TARGETS="${CTARGET}"
EOF

In this fictitious example (we've stopped using CTARGET), CTARGET is a locked
variable specified by the C multibuild class for all targets.

We also need to make sure the primary build target's install target gets executed last
in case binaries collide. Installing a 32 bit /sbin/ldconfig won't result in a pleasant
outcome. We currently enforce this through the multibuild_sanity_check_profile hack in
multibuild.exlib, but when we turn to choices that solution probably isn't viable.


4. easy-multibuild.exlib
An awful lot of exheres end up with an awful lot of similar for loops with that
approach.
So to remedy the trivial cases easy-multibuild.exlib was added.

This exlib exports src_configure, src_compile, src_test and src_install. In each
of these it runs the for loop from the examples in 4.1 and 4.2.

Inside the for loop it:
    1. Switches to the right build target.
    2. Then it looks for ${EXHERES_PHASE}_prepare_one_multibuild and runs it if
       it is defined. This is useful for packages using ECONF_SOURCE that need
       something done in ${WORK} before switching the build target specific dir.
    3. Then it switches to the correct build dir.
    4. Then it looks for ${EXHERES_PHASE}_one_multibuild and runs it if it is defined.
       Otherwise it runs default.
    5. Then it pops back to ${WORK}.

Currently it only supports the C build class and sets that by default but it should
be easy to add other classes.

The following is an example of using this exlib:

require easy-multibuild

[...]

src_install() {
    easy-multibuild_src_install

    edo mv "${IMAGE}"/usr/share/doc/${PN}/* "${IMAGE}"/usr/share/doc/${PNVR}/
    edo rmdir "${IMAGE}"/usr/share/doc/${PN}/
}


II. Our general approach to multilib

1. Making exheres "multibuild-class-aware"
As described in "notes" [2] the repository, through the respective profile,
defines a set of multibuild class variables, plus permissible values for them.
These are passed through to the exheres by means of variables.

2. Multibuild classes and values
For different kinds of what we used to call ABIs, say Perl or Python "ABI"s in
contrast to C, we have the concept of multibuild classes:
MULTIBUILD_CLASSES="C python", for example, and MULTIBUILD_C, MULTIBUILD_PYTHON,
etc..

For every kind of multibuild class, we have its respective targets, e. g. for C
on x86_64/i686 (aka amd64/x86) we have the multibuild class "C" and its
permissable targets "32" and "64".
For python we'll have the multibuild class "python" and its permissible targets
"26" and "31". Basically values based on ${SLOT/.} for the python exheres, etc.

[Editor's note: I have no idea about Python, Perl, Ruby, or whatever kind of
stuff you might come up with. Please help us out here if you have a
clue.]


3. Multibuild class handling in exheres
Exheres-side handling has to be done explicitly, though magic exlibs can help with
this. The handling we're currently envisioning is shown in I. 2.ff.
Of course, the repetitive switching of multibuild class-specific subdirs and
multibuild classes themselves should be generalised in an exlib as has been begun
by easy-multibuild.exlib as demonstrated in I., 5..


4. Multibuild classes and dependencies
Exheres should explicitly state which multibuild classes and targets they support,
disregarding the precise syntax at this point, this would work something like this:

MYOPTIONS="multibuild_classes: C".

This means, the package supports all multibuild targets in the MULTIBUILD_C class.
This is the default.
Only if a package, as an exception, doesn't support one of the multibuild targets
of the multibuild class, we'd express this explicitly like this (again, only an
example of how it should work, not necessarily the way it will be expressed):

MYOPTIONS="multibuild_classes: C=32".

It will be the resolver's responsibility to work out how to satisfy the dependencies
in case additional multibuild targets from a given multibuild class are requested
by the user.

This allows us to have dependencies pulled in according to the multibuild targets
(per multibuild class) we've set for any given package.

Thus, dependencies default to MULTIBUILD_<class>_DEFAULT_TARGET and whatever
additional multibuild targets per class have been specified.

Choosing which multibuild targets to install is done as follows:
 - If a single or a set of multibuild target(s) is explicitly requested, use
   that/those; the resolver needs to complain if this would conflict with other
   dependencies.
 - If not, start with an empty set.
 - If the package is already installed, then add the set of multibuild targets
   with which it was installed last.
 - If the package is not already installed, then add the set of default multibuild
   targets and any multibuild targets additionally requested.
 - When building the dependency list, any multibuild target explicitly depended
   upon for this package is added to the set.

The primary multibuild target is determined thus:
 - If only one multibuild target is to be installed, then that is the primary
   multibuild target.
 - Else the system default multibuild target is used. If necessary, add this to
   the list of multibuild targets to be installed.


III. (Half-)Open issues

1. The CHOST variable, -m<abi> switches and their implications
In our proof of concept, we've been having discussions about the correct setting
of the multibuild-target-dependent CHOST value, e. g. if CHOST_32 should be set
to the real host's CHOST value (e. g. x86_64-pc-linux-gnu) or the target's value
(e.g. i686-pc-linux-gnu).

Some packages expect CHOST to hold the native value and -m<abi> switches to do
"the right thing". Others work fine if CHOST holds the target value.

More interestingly, this author's favourite crappy arch, mips, was brought into
the discussion as well as it has two ABIs needing mips64-* as a CHOST and one
needing mips-*. All of them need -m<abi> flags.

Thus, what should we set CHOST_<abi> to? The target or the real host's multibuild
target?

2. The -m<abi> switches should come from the profile but there's a set of options
to handle that:

User config and multibuild-dependent vars: what happens if a user sets
CFLAGS="-foo" in bashrc? Options:
 1) Disallow it completely. Irritating for the user, especially on
    systems that only have one multibuild target.
 2) Let it apply to the "default" multibuild target. Convenient on single-
    multibuild-target systems, but somewhat annoying on real multilib ones.
    Slightly confusing.
 3) Apply it to all multibuild targets. Approaches what the user expects, but could
    cause problems when multibuild targets are distinguished only by compiler flags
    (mips -mabi=n32, for example).
 4) Apply it to all multibuild targets, and have an extra multibuild-target-
    dependent variable for profile-set multibuild-target-relevant flags, which
    cannot be overridden globally and is appended to CFLAGS on multibuild target
    switches.

So far, we've basically gone with implementing this:

Let the user set MULTIBUILD_<class>_<target>_USER_<*FLAGS> which apply to the
specific class and target only. These get merged with profile-specific *FLAGS and
finally any global C{,XX}FLAGS get merged into this as well.

This approach has been implemented in multibuild.exlib by zlin.


3. We can't rely on the primary multibuild target happening to be last in the list
The list of multibuild targets in a given multibuild class to be built will be
determined by the resolver, not the profile. One way to deal with this would be
to split out primary-multibuild-target and other-multibuild-targets into different
exheres env vars.
Another but strongly unadvisable way would be to have Paludis magically put the
primary multibuild target last.


IV. Package Manager Support

As far as I can tell, all the functions in multibuild.exlib should be in
exheres-0 because I don't think we want exheres to need to require an exlib in
order to get values of simple things like CFLAGS, CHOST and LIBDIR. Right
after profiles are sourced and before an exheres is sourced, I think the
default C target should be loaded.

Choices support.


Footnotes:

[1] http://git.exherbo.org/?p=arbor.git;a=blob;f=profiles/x86_64-pc-linux-gnu/make.defaults;hb=refs/heads/multilib
[2] http://git.exherbo.org/?p=exherbo.git;a=blob;f=scratch/multilib/notes;hb=HEAD
[3] http://git.exherbo.org/?p=arbor.git;a=blob;f=exlibs/multibuild.exlib;hb=refs/heads/multilib
[4] http://git.exherbo.org/?p=arbor.git;a=blob;f=packages/sys-devel/binutils/binutils.exlib;hb=refs/heads/multilib
